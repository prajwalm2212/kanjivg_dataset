# kanjivg_dataset

Dataset for training models in teaching robots to draw.

## Visualization of Global Model

![global model Visualization](res/global_model_vis.gif)

### Setting up working Directory

```
$tar -xf kanjivg.tar.gz # extract original dataset of characters
$mkdir kanji_modified   # Directory to store converted characters
$mkdir test_dir
$mkdir test_dir/global_pics
$mkdir test_dir/local_pics
$pip3 install -r requirements.txt # install all dependencies
```

## Files in this Repository and their respective functions

1. **global_strokegenerator.py** --> implements a python generator which yields samples to train global model,also provides feature to use data augmentation.

2. **drawing_utils.py** --> module with some basic drawing functions and user defined datatypes ex : class Point and ImageGen.

3. **extract_kanjivg.py** --> file to extract character defined in complex bezeir curves into simple line segements for easier processing.

4. **visualize_dataset.py** --> visualise dataset by calling stroke generators for respective datasets

5. **bresenhamsalgo.py** --> divides a line into independent points, taking into account of pixel error.

6. **local_strokegenerator.py** --> simple python generator that yields data required for training local model
